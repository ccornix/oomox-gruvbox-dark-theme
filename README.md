# oomox-gruvbox-dark

Dark GTK2/3 and icon themes featuring [gruvbox][gruvbox] colors; created using [Oomox][oomox], based on the (Numix-based) Oomox GTK theme and [Papirus icon theme][papirus-icons], respectively.

![](gtk-theme-preview.png) ![](icon-theme-preview.png)

[gruvbox]: https://github.com/morhetz/gruvbox
[oomox]: https://github.com/themix-project/oomox
[papirus-icons]: https://github.com/PapirusDevelopmentTeam/papirus-icon-theme
